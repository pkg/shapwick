# Makefile to build all of the service code in the fi/ subdirectories. This uses
# non-recursive automake to allow the services to be built in parallel, and
# allow sharing of build rules in this file.

# To add a new service:
#  1. Add a section for it to this file, appending to lib_LTLIBRARIES and
#     includesfi_HEADERS.
#  2. Add it to the library list in docs/reference/Makefile.am.
#  3. Add its documentation to the includes in
#     docs/reference/canterbury-docs.xml.
#  4. Add it to the library list and CPPFLAGS in sources/Makefile.am.
# Note that as this is all generated code, we disable various warnings.

AM_CPPFLAGS = \
	-include config.h \
	$(NULL)

AM_CFLAGS = \
	$(WARN_CFLAGS) \
	$(NULL)

AM_LDFLAGS = \
	-no-undefined \
	$(WARN_LDFLAGS) \
	$(NULL)

service_cflags = \
	$(GLIB_CFLAGS) \
	$(CODE_COVERAGE_CFLAGS) \
	$(AM_CFLAGS)
service_libadd = \
	$(GLIB_LIBS) \
	$(CODE_COVERAGE_LIBS) \
	$(AM_LIBADD)
service_ldflags = \
	$(AM_LDFLAGS)
service_codegen_flags = \
	--generate-docbook docs \
	--c-namespace=Shapwick \
	--interface-prefix=org.apertis.Shapwick \
	$(NULL)

# Generic rules
%.c %.h docs-%.xml: %.xml
	$(AM_V_GEN)$(GDBUS_CODEGEN) \
		$(service_codegen_flags) --generate-c-code $* $<

shapwick_sources_h = $(shapwick_sources_xml:.xml=.h)
shapwick_sources_c = $(shapwick_sources_xml:.xml=.c)
shapwick_built_docs = $(addprefix docs-,*)

EXTRA_DIST = $(shapwick_sources_xml)
CLEANFILES = $(shapwick_sources_h) $(shapwick_sources_c) $(shapwick_built_docs)

includesfidir = $(includedir)/shapwick
nodist_includesfi_HEADERS = $(shapwick_sources_h)

# shapwick-app-db-handler
shapwick_sources_xml =org.apertis.Shapwick.xml

lib_LTLIBRARIES = libshapwickiface.la
nodist_libshapwickiface_la_SOURCES = \
	$(shapwick_sources_xml:.xml=.c) \
	$(shapwick_sources_xml:.xml=.h) \
	$(NULL)

libshapwickiface_la_CFLAGS = $(service_cflags)
libshapwickiface_la_LIBADD = $(service_libadd)
libshapwickiface_la_LDFLAGS = $(service_ldflags)

-include $(top_srcdir)/git.mk
