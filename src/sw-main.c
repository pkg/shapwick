/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "sw-private.h"

//#include "app_handler.h"
#include <libcgroup.h>

ShapwickStrct ShapwickObj;

gboolean
is_cgroup_subsystem_enabled (const gchar * name)
{
  int hierarchy, num_cgroups, enabled;
  FILE *fd;
  char subsys_name[FILENAME_MAX];
  gboolean return_val = FALSE;

  fd = fopen ("/proc/cgroups", "r");
  if (!fd)
    return FALSE;

  while (!feof (fd))
    {
      fscanf (fd, "%s, %d, %d, %d", subsys_name,
              &hierarchy, &num_cgroups, &enabled);

      if (g_strcmp0 (name, subsys_name) == FALSE)
        return_val = TRUE;
    }

  fclose (fd);

  return return_val;
}

static struct cgroup *
create_new_cgroup (const gchar * pGroupName, const gchar * pControllerName,
                   const gchar * pMoveChargeAtImmigrate)
{
  struct cgroup *pNewCgroup = NULL;
  struct cgroup_controller *pNewController;

  gint error;

  error = cgroup_init ();

  if (error)
    {
      g_printerr ("cgroup_init failed with %s\n", cgroup_strerror (error));
      exit (1);
    }

  res_mgr_debug ("create a cgroup **%s** under the controller **%s** \n",
                 pGroupName, pControllerName);

  pNewCgroup = cgroup_new_cgroup (pGroupName);

  cgroup_set_uid_gid (pNewCgroup, 0, 0, 0, 0);
  pNewController = cgroup_add_controller (pNewCgroup, pControllerName);

  if ((pMoveChargeAtImmigrate != NULL) &&
      (cgroup_set_value_bool (pNewController, pMoveChargeAtImmigrate, 1)))
    {
      g_printerr ("pMoveChargeAtImmigrate failed \n");
    }

  if (cgroup_create_cgroup (pNewCgroup, 0) == 0)
    {
      res_mgr_debug ("new cgroup  %s created \n", pGroupName);
    }
  return pNewCgroup;
}

static gboolean
add_pid_to_bandwidth_cgroup (ShapwickBandwidth * object,
                             GDBusMethodInvocation * invocation,
                             guint uinPid,
                             guint uinBandwidthPrio, gpointer pUserData)
{
  gint retval = 0;

  bandwidth_debug ("add_pid_to_bandwidth_cgroup pid %d prio %d \n", uinPid,
                   uinBandwidthPrio);
  shapwick_bandwidth_complete_add_process_to_bandwidth_cgroup (object,
                                                                   invocation);

  if (uinPid)
    {
      switch (uinBandwidthPrio)
        {
        case SHAPWICK_LOWEST:
          retval =
            cgroup_attach_task_pid (ShapwickObj.lowest_prio_cgroup, uinPid);
          break;

        case SHAPWICK_LOW:
          retval =
            cgroup_attach_task_pid (ShapwickObj.low_prio_cgroup, uinPid);
          break;

        case SHAPWICK_MID:
          retval =
            cgroup_attach_task_pid (ShapwickObj.mid_prio_cgroup, uinPid);
          break;

        case SHAPWICK_HIGH:
          {
            bandwidth_debug ("high prio source\n");
            retval =
              cgroup_attach_task_pid (ShapwickObj.high_prio_cgroup, uinPid);
          }
          break;

        case SHAPWICK_HIGHEST:
          retval =
            cgroup_attach_task_pid (ShapwickObj.highest_prio_cgroup, uinPid);
          break;

        default:
          g_printerr ("unknown priority sent by app-mgr \n");
          break;
        }
    }

  if (retval != 0)
    {
      g_printerr ("process couldnt be attached to cgroup \n");
    }

  return TRUE;
}

static void
on_bandwidth_bus_acquired (GDBusConnection * connection,
                           const gchar * name, gpointer pUserData)
{
  bandwidth_debug ("on_bus_acquired %s \n", name);

  if (is_cgroup_subsystem_enabled (CGROUP_SUBSYSTEM_BANDWIDTH) == FALSE)
  {
	  g_printerr ("bandwidth controller is not enabled in kernel \n");
	  return;
  }

  /* here we export all the methods supported by the service, this callback is called before on_name_acquired */
  /* once on_name_acquired is called clients can start calling service interfaces, so its important to export all methods here */

  /* Creates a new skeleton object, ready to be exported */
  ShapwickObj.pBandwidthObject = shapwick_bandwidth_skeleton_new ();

  /* provide signal handlers for all methods */
  g_signal_connect (ShapwickObj.pBandwidthObject,
                    "handle-add-process-to-bandwidth-cgroup",
                    G_CALLBACK (add_pid_to_bandwidth_cgroup), pUserData);

  /* Exports interface launcher_object at object_path "/Sample/Service" on connection */
  if (!g_dbus_interface_skeleton_export
      (G_DBUS_INTERFACE_SKELETON (ShapwickObj.pBandwidthObject), connection,
       "/org/apertis/Shapwick/Bandwidth", NULL))
    {
      g_printerr ("export error \n");
    }
  //bandwidth management init fun
  ShapwickObj.lowest_prio_cgroup =
    create_new_cgroup (CGROUP_SUBGROUP_BANDWIDTH_LOWEST,
                       CGROUP_SUBSYSTEM_BANDWIDTH, NULL);
  ShapwickObj.low_prio_cgroup =
    create_new_cgroup (CGROUP_SUBGROUP_BANDWIDTH_LOW,
                       CGROUP_SUBSYSTEM_BANDWIDTH, NULL);
  ShapwickObj.mid_prio_cgroup =
    create_new_cgroup (CGROUP_SUBGROUP_BANDWIDTH_MID,
                       CGROUP_SUBSYSTEM_BANDWIDTH, NULL);
  ShapwickObj.high_prio_cgroup =
    create_new_cgroup (CGROUP_SUBGROUP_BANDWIDTH_HIGH,
                       CGROUP_SUBSYSTEM_BANDWIDTH, NULL);
  ShapwickObj.highest_prio_cgroup =
    create_new_cgroup (CGROUP_SUBGROUP_BANDWIDTH_HIGHEST,
                       CGROUP_SUBSYSTEM_BANDWIDTH, NULL);
}

static void
on_bandwidth_name_acquired (GDBusConnection * connection,
                            const gchar * name, gpointer pUserData)
{
  bandwidth_debug ("on_name_acquired %s \n", name);
}

static void
on_bandwidth_name_lost (GDBusConnection * connection,
                        const gchar * name, gpointer pUserData)
{
  /* free all dbus handlers ... */
  bandwidth_debug ("on_name_lost \n");
}

static void
on_filesystem_bus_acquired (GDBusConnection * connection,
                            const gchar * name, gpointer pUserData)
{
  filesystem_debug ("on_bus_acquired %s \n", name);

  /* here we export all the methods supported by the service, this callback is called before on_name_acquired */
  /* once on_name_acquired is called clients can start calling service interfaces, so its important to export all methods here */

  /* Creates a new skeleton object, ready to be exported */
  ShapwickObj.pFileSystemObject = shapwick_file_system_skeleton_new ();

  fileshapwick_init (ShapwickObj.pFileSystemObject);

  if (!g_dbus_interface_skeleton_export
      (G_DBUS_INTERFACE_SKELETON (ShapwickObj.pFileSystemObject), connection,
       "/org/apertis/Shapwick/FileSystem", NULL))
    {
      g_printerr ("export error \n");
    }
}

static void
on_filesystem_name_acquired (GDBusConnection * connection,
                             const gchar * name, gpointer pUserData)
{
  filesystem_debug ("on_name_acquired %s \n", name);
}

static void
on_filesystem_name_lost (GDBusConnection * connection,
                         const gchar * name, gpointer pUserData)
{
  /* free all dbus handlers ... */
  filesystem_debug ("on_name_lost \n");
}

static gboolean
add_pid_to_memory_cgroup (ShapwickMemory * object,
                          GDBusMethodInvocation * invocation,
                          guint uinPid, gpointer pUserData)
{
  gint retval = 0;

  memory_debug ("add_pid_to_memory_cgroup %d \n", uinPid);
  shapwick_memory_complete_add_process_to_memory_cgroup (object,
                                                             invocation);


  if (uinPid)
    retval = cgroup_attach_task_pid (ShapwickObj.pMemoryCgroup, uinPid);

  if (retval != 0)
    {
      g_printerr ("process couldnt be attached to cgroup \n");
    }
  return TRUE;
}

static gboolean
register_for_oom_notification (ShapwickMemory * object,
                               GDBusMethodInvocation * invocation,
                               gpointer pUserData)
{
  memory_debug ("register_for_oom_notification \n");
  shapwick_memory_complete_register_oomnotification (object, invocation);

  if (NULL ==
      (g_thread_new
       ("b_memory_resource_mgr_thread", memory_resource_mgr_thread, NULL)))
    {
      g_printerr ("b_memory_resource_mgr_thread creation failed \n");
    }

  return TRUE;
}

static void
on_memory_bus_acquired (GDBusConnection * connection,
                        const gchar * name, gpointer pUserData)
{
  memory_debug ("on_bus_acquired %s \n", name);

  if (is_cgroup_subsystem_enabled (CGROUP_SUBSYSTEM_MEMORY) == FALSE)
  {
	  g_printerr ("memory controller is not enabled in kernel \n");
	  return ;
  }

  /* here we export all the methods supported by the service, this callback is called before on_name_acquired */
  /* once on_name_acquired is called clients can start calling service interfaces, so its important to export all methods here */

  /* Creates a new skeleton object, ready to be exported */
  ShapwickObj.pMemoryObject = shapwick_memory_skeleton_new ();

  /* provide signal handlers for all methods */
  g_signal_connect (ShapwickObj.pMemoryObject,
                    "handle-add-process-to-memory-cgroup",
                    G_CALLBACK (add_pid_to_memory_cgroup), pUserData);

  g_signal_connect (ShapwickObj.pMemoryObject,
                    "handle-register-oomnotification",
                    G_CALLBACK (register_for_oom_notification), pUserData);

  if (!g_dbus_interface_skeleton_export
      (G_DBUS_INTERFACE_SKELETON (ShapwickObj.pMemoryObject), connection,
       "/org/apertis/Shapwick/Memory", NULL))
    {
      g_printerr ("export error \n");
    }

  //memory management init fun
  ShapwickObj.pMemoryCgroup =
    create_new_cgroup (CGROUP_SUBGROUP_MEMORY, CGROUP_SUBSYSTEM_MEMORY,
                       CGROUP_MEMORY_MOVE_CHARGE);
  // please create here memory's Application cgroup and also initialize the overall 1GB memory monitoring in a separate thread
}

static void
on_memory_name_acquired (GDBusConnection * connection,
                         const gchar * name, gpointer pUserData)
{
  memory_debug ("on_name_acquired %s \n", name);
}

static void
on_memory_name_lost (GDBusConnection * connection,
                     const gchar * name, gpointer pUserData)
{
  /* free all dbus handlers ... */
  memory_debug ("on_name_lost \n");
}


static void
shapwick_on_bus_acquired (GDBusConnection * connection,
                           const gchar * name, gpointer pUserData)
{

	on_memory_bus_acquired(connection , name , pUserData);
	on_bandwidth_bus_acquired(connection , name , pUserData);
	on_filesystem_bus_acquired(connection , name , pUserData);
}

static void
shapwick_on_name_acquired (GDBusConnection * connection,
                           const gchar * name, gpointer pUserData)
{

	on_memory_name_acquired(connection , name , pUserData);
	on_bandwidth_name_acquired(connection , name , pUserData);
	on_filesystem_name_acquired(connection , name , pUserData);
}


static void
shapwick_on_name_lost (GDBusConnection * connection,
                           const gchar * name, gpointer pUserData)
{

	on_memory_name_lost(connection , name , pUserData);
	on_bandwidth_name_lost(connection , name , pUserData);
	on_filesystem_name_lost(connection , name , pUserData);
}



gint
main (gint argc, gchar * argv[])
{
  GMainLoop *mainloop = NULL;

  guint owner_id;

  owner_id = g_bus_own_name (G_BUS_TYPE_SYSTEM,     // bus type
                             "org.apertis.Shapwick", // interface name
                             G_BUS_NAME_WATCHER_FLAGS_AUTO_START,   // bus own flag, can be used to take away the bus and give it to another service
                             shapwick_on_bus_acquired,        // callback invoked when the bus is acquired
                             shapwick_on_name_acquired,       // callback invoked when interface name is acquired
                             shapwick_on_name_lost,   // callback invoked when name is lost to another service or other reason
                             NULL,  // user data
                             NULL);




  register_to_net_connman ();

  mainloop = g_main_loop_new (NULL, FALSE);
  g_main_loop_run (mainloop);
  g_bus_unown_name (owner_id);
  exit (0);
}
