/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/*
 * connman_interface.c
 *
 */

#include <stdlib.h>
#include <string.h>
#include "sw-private.h"
#include <libcgroup.h>
#include "connman-manager-glue.h"

NetConnmanManager *net_connman_mng_proxy;

static void
set_value_for_net_prio_cgroup (const gchar * interface_name,
                               const gchar * pGroupName,
                               const gchar * pControllerName)
{
  struct cgroup *pNewCgroup = NULL;
  struct cgroup_controller *pNewController;
  gchar *map = NULL;
  gint error;

  error = cgroup_init ();

  if (error)
    {
      g_printerr ("net_prio cgroup_init failed with %s\n",
                  cgroup_strerror (error));
      exit (1);
    }

  pNewCgroup = cgroup_new_cgroup (pGroupName);

  pNewController = cgroup_add_controller (pNewCgroup, pControllerName);

  if (g_strcmp0 (CGROUP_SUBGROUP_BANDWIDTH_LOWEST, pGroupName) == 0)
    {
      map = g_strdup_printf ("%s 1", interface_name);
    }
  else if (g_strcmp0 (CGROUP_SUBGROUP_BANDWIDTH_LOW, pGroupName) == 0)
    {
      map = g_strdup_printf ("%s 2", interface_name);
    }
  else if (g_strcmp0 (CGROUP_SUBGROUP_BANDWIDTH_MID, pGroupName) == 0)
    {
      map = g_strdup_printf ("%s 4", interface_name);
    }
  else if (g_strcmp0 (CGROUP_SUBGROUP_BANDWIDTH_HIGH, pGroupName) == 0)
    {
      map = g_strdup_printf ("%s 6", interface_name);
    }
  else if (g_strcmp0 (CGROUP_SUBGROUP_BANDWIDTH_HIGHEST, pGroupName) == 0)
    {
      map = g_strdup_printf ("%s 7", interface_name);
    }
  else
    {
      g_printerr ("unknown bandwidth group name found \n");
    }

  g_print ("CGROUP_BANDWIDTH_PRIO_MAP %s \n", map);
  if (cgroup_set_value_string
      (pNewController, CGROUP_BANDWIDTH_PRIO_MAP, map))
    {
      g_printerr ("CGROUP_BANDWIDTH_PRIO_MAP failed \n");
    }

  if (cgroup_modify_cgroup (pNewCgroup) != 0)
    {
      res_mgr_debug (" %s cgroup modification failed \n", pGroupName);
    }

  g_free (map);
}

static void
set_priority_for_interface (const gchar * interface_name)
{

  set_value_for_net_prio_cgroup (interface_name,
                                 CGROUP_SUBGROUP_BANDWIDTH_LOWEST,
                                 CGROUP_SUBSYSTEM_BANDWIDTH);
  set_value_for_net_prio_cgroup (interface_name,
                                 CGROUP_SUBGROUP_BANDWIDTH_LOW,
                                 CGROUP_SUBSYSTEM_BANDWIDTH);
  set_value_for_net_prio_cgroup (interface_name,
                                 CGROUP_SUBGROUP_BANDWIDTH_MID,
                                 CGROUP_SUBSYSTEM_BANDWIDTH);
  set_value_for_net_prio_cgroup (interface_name,
                                 CGROUP_SUBGROUP_BANDWIDTH_HIGH,
                                 CGROUP_SUBSYSTEM_BANDWIDTH);
  set_value_for_net_prio_cgroup (interface_name,
                                 CGROUP_SUBGROUP_BANDWIDTH_HIGHEST,
                                 CGROUP_SUBSYSTEM_BANDWIDTH);

}

static gchar *
filter_for_interface_name (GVariant * network_list)
{
  const gchar *interface_name_temp = NULL;

  if (network_list != NULL)
    {
      gchar *name2 = NULL, *field = NULL;
      GVariantIter iter, array_iter;
      GVariant *array = NULL, *array_value = NULL;
      GVariantIter *fields;
      GVariant *value1 = NULL;

      g_variant_iter_init (&iter, network_list);
      array = g_variant_iter_next_value (&iter);
      g_variant_iter_init (&array_iter, array);
      while ((array_value = g_variant_iter_next_value (&array_iter)) != NULL)
        {
          g_variant_get (array_value, "(oa{sv})", &name2, &fields);
          while (g_variant_iter_next (fields, "{sv}", &field, &value1))
            {
              if ((g_strcmp0 (field, "Ethernet") == 0))
                {
                  GVariantIter *ethernet;
                  gchar *interface;
                  GVariant *interface_value;

                  g_variant_get (value1, "a{sv}", &ethernet);
                  while (g_variant_iter_next
                         (ethernet, "{sv}", &interface, &interface_value))
                    {
                      if (g_strcmp0 (interface, "Interface") == 0)
                        {
                          interface_name_temp =
                            g_variant_get_string (interface_value, NULL);
                        }
                    }

                }
            }
        }
    }

  return g_strdup (interface_name_temp);
}

static void
net_connman_property_changed (NetConnmanManager * object,
                              const gchar * arg_unnamed_arg0,
                              GVariant * arg_unnamed_arg1)
{
  GVariant *returned_service_list = NULL;
  GError *error = NULL;
  gchar *interface_name = NULL;

  GVariant *temp_variant = g_variant_get_variant (arg_unnamed_arg1);

  if (g_variant_is_of_type (temp_variant, G_VARIANT_TYPE_STRING))
    {
      const gchar *state_is = g_variant_get_string (temp_variant, NULL);

      bandwidth_debug (" State = %s\n", state_is);

      if (g_strcmp0 (state_is, "online") == 0)
        {
          GVariant *network_list = NULL;
          //TODO: Add priority for interface

          net_connman_manager_call_get_services_sync (net_connman_mng_proxy,
                                                      &returned_service_list,
                                                      NULL, &error);
          if (error)
            {
              g_warning ("Get Service sync failed %s",
                         error ? error->message : "no error given.");
              g_error_free (error);
              error = NULL;
            }
          else
            bandwidth_debug (" GetServices success \n");

          network_list =
            g_variant_new ("(@a(oa{sv}))", returned_service_list);
          interface_name = filter_for_interface_name (network_list);
          bandwidth_debug (" interface Name = %s\n", interface_name);
          set_priority_for_interface (interface_name);
          g_free (interface_name);
        }
    }
}

static void
net_connman_manager_proxy_created_clb (GObject * source_object,
                                       GAsyncResult * res, gpointer user_data)
{
  GError *error = NULL;

  /* finishes the proxy creation and gets the proxy ptr */
  net_connman_mng_proxy = net_connman_manager_proxy_new_finish (res, &error);

  if ((net_connman_mng_proxy == NULL) || error)
    {
      g_warning ("Error intializing the net.connman mng proxy %s",
                 error ? error->message : "no error given.");
      // exit(0);
    }
  else
    {
      g_signal_connect (net_connman_mng_proxy, "property-changed",
                        G_CALLBACK (net_connman_property_changed), NULL);
    }
}

static void
net_connman_name_appeared (GDBusConnection * connection,
                           const gchar * name,
                           const gchar * name_owner, gpointer user_data)
{
  bandwidth_debug ("net.connman name appeared \n");
  net_connman_manager_proxy_new (connection, G_DBUS_CALL_FLAGS_NONE,
                                 "net.connman", "/", NULL,
                                 net_connman_manager_proxy_created_clb,
                                 user_data);
}

static void
net_connman_name_vanished (GDBusConnection * connection,
                           const gchar * name, gpointer user_data)
{
  bandwidth_debug ("net.connman name vanished \n");
}

void
register_to_net_connman (void)
{
  g_bus_watch_name (G_BUS_TYPE_SYSTEM,
                    "net.connman",
                    G_BUS_NAME_WATCHER_FLAGS_AUTO_START,
                    net_connman_name_appeared,
                    net_connman_name_vanished, NULL, NULL);

}
